import numpy as np
from numpy import linalg

class Point:
    
    # Initialization of a new point from a given string by splitting components
    def __init__(self, line):
        values = line.split(",")
        self.components = np.array([round(float(k), 5) for k in values])
        self.number_of_points = 1
        self.objective_function_value = 0

    # Add a new point to this point and calculate sum and objective function value
    def sum(self, p, h):
        self.components = np.add(self.components, p.components)
        self.number_of_points += p.number_of_points
        self.objective_function_value += pow(self.distance(p, h), 2)
        return self

    # Calculate the distance between this point and a given point
    def distance(self, p, h):
        if (h < 0):
           h = 2
        return round(linalg.norm(self.components - p.components, h), 5)

    # Realign center point
    def get_average_point(self):
        self.components = np.around(np.divide(self.components, self.number_of_points), 5)
        return self
    
    def __str__(self):
        result = ""
        for component in self.components:
            result += str(component)
            result += " "
        return result.strip()

    def __repr__(self):
        # Spark uses this method when save on text file
        result = ""
        for component in self.components:
            result += str(component)
            result += " "
        return result.strip()