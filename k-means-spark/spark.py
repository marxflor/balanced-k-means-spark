import sys
import os
import json
from pyspark import SparkContext
from pyspark.sql import SparkSession
from point import Point
import time

import numpy as np

# Calculate initial centroids
def init_centroids(dataset, k):
    start_time = time.time()
    initial_centroids = dataset.takeSample(False, k)
    print("init centroid execution:", len(initial_centroids), "in", (time.time() - start_time), "s")
    return initial_centroids

# Calculate initial centroids calculation by kmeans++
def init_centroids_kmeans_pp(dataset, k, h):
    # Randomly select first centroid
    # Loop k - 1 times
        # Calculate distance of each point relatively to all centroids
        # Choose point with max distance as new centroid
    start_time = time.time()
    initial_centroids = dataset.takeSample(False, 1)
    dataArray = dataset.collect()
    while len(initial_centroids) < k:
        distances = np.zeros(len(dataArray))
        for i in range(len(dataArray)):
            p = dataArray[i]
            min_dist = float("inf")
            for j in range(len(initial_centroids)):
                dist = p.distance(initial_centroids[j], h)
                if dist < min_dist:
                    min_dist = dist
            distances[i] = min_dist
        initial_centroids.append(dataArray[np.argmax(distances)])
    print("init centroid execution:", len(initial_centroids), "in", (time.time() - start_time), "s")
    return initial_centroids

# Assign a point to a cluster with a centroid
def assign_centroids(p):
    min_dist = float("inf")
    centroids = centroids_broadcast.value
    nearest_centroid = 0
    for i in range(len(centroids)):
        distance = p.distance(centroids[i], distance_broadcast.value)
        if(distance < min_dist):
            min_dist = distance
            nearest_centroid = i
    return (nearest_centroid, p)

# Assign a point to a cluster with a centroid in the balanced k-means algorithm
def assign_centroids_balanced(p, points_amount):
    assignments = assignments_broadcast.value
    return (assignments[repr(p.components)], p)

# Create an assignment list of points for each cluster with use of the distance matrix
def create_assignment_dict(points_collected, distance_matrix):
    centroids = centroids_broadcast.value
    assignments = {}
    num_rows, num_cols = distance_matrix.shape

    points_amount = len(points_collected)
    
    balanced_amount = round(points_amount / len(centroids))
    distance_matrix = distance_matrix_broadcast.value

    for i in range(num_rows - 1):
        min_values_indices = distance_matrix[i].argsort()[:balanced_amount]
        for j in min_values_indices:
            assignments[repr(points_collected[j].components)] = i
            for k in range(num_rows):
                distance_matrix[k][j] = float("NaN")
            
    # last row
    for i in range(num_cols):
        if np.isnan(distance_matrix[num_rows - 1][i]) == False:
            assignments[repr(points_collected[i].components)] = num_rows - 1
    
    return assignments

# Create a matrix that describes the distance of every point to every centroid
def create_distance_matrix(points_collected):
    centroids = centroids_broadcast.value
    distance_matrix = np.zeros((len(centroids), points.count()))
    for i in range(len(centroids)):
        for j in range(points.count()):
            distance_matrix[i][j] = centroids[i].distance(points_collected[j], distance_broadcast.value)
    return distance_matrix

# Get the sum of all objective function values of all centroids
def get_objective_function_value(centroids):
    objective_function_value = 0
    for i in range(len(centroids)):
        objective_function_value += centroids[i].objective_function_value
    return objective_function_value

# Stopping criterion for normal k means
def stopping_criterion(new_centroids, threshold):
    old_centroids = centroids_broadcast.value
    for i in range(len(old_centroids)):
        check = old_centroids[i].distance(new_centroids[i], distance_broadcast.value) <= threshold
        if check == False:
            return False
    return True

# Stopping criterion for balanced k means
def stopping_criterion_balanced(new_centroids):
    old_centroids = centroids_broadcast.value
    objective_value_new = get_objective_function_value(new_centroids)
    objective_value_old = get_objective_function_value(old_centroids)
    if objective_value_old == 0:
        return False
    return objective_value_new > objective_value_old

if __name__ == "__main__":
    start_time = time.time()
    if len(sys.argv) != 3:
        print("Number of arguments not valid!")
        sys.exit(1)

    with open(os.path.join(os.path.dirname(__file__),"./config.json")) as config:
        parameters = json.load(config)["configuration"][0]

    INPUT_PATH = str(sys.argv[1])
    OUTPUT_PATH = str(sys.argv[2])
    
    spark = SparkSession.builder.appName("PySparkkmeans").getOrCreate()
    sc = spark.sparkContext
    #sc = SparkContext("yarn", "Kmeans")
    sc.setLogLevel("ERROR")
    sc.addPyFile(os.path.join(os.path.dirname(__file__),"./point.py")) ## It's necessary, otherwise the spark framework doesn't see point.py

    print("\n***START****\n")

    points = sc.textFile(INPUT_PATH).map(Point).cache()
    #initial_centroids = init_centroids(points, k=parameters["k"])
    initial_centroids = init_centroids_kmeans_pp(points, k=parameters["k"], h=parameters["distance"])
    distance_broadcast = sc.broadcast(parameters["distance"])
    centroids_broadcast = sc.broadcast(initial_centroids)

    stop, n = False, 0
    while True:
        print("--Iteration n. {itr:d}".format(itr=n+1), end="\r", flush=True)

        # collect points
        points_collected = points.collect()

        # calculate distance matrix
        distance_matrix = create_distance_matrix(points_collected)
        distance_matrix_broadcast = sc.broadcast(distance_matrix)

        # calculate point assignments
        assignments = create_assignment_dict(points_collected, distance_matrix)
        assignments_broadcast = sc.broadcast(assignments)

        # assign points to cluster
        points_amount = points.count()
        cluster_assignment_rdd = points.map(lambda j: assign_centroids_balanced(j, points_amount))

        # calculate sum of all points in each cluster with the corresponding objective function value
        sum_rdd = cluster_assignment_rdd.reduceByKey(lambda x, y: x.sum(y, distance_broadcast.value))

        # update centroids
        # new center of cluster i is (1 / amount of nodes in cluster i) * (sum of points in cluster i)
        centroids_rdd = sum_rdd.mapValues(lambda x: x.get_average_point()).sortByKey(ascending=True)
        new_centroids = [item[1] for item in centroids_rdd.collect()]

        # Stopping criterion for termination
        # stop = stopping_criterion(new_centroids, parameters["threshold"])
        stop = stopping_criterion_balanced(new_centroids)

        n += 1
        if(stop == False and n < parameters["maxiteration"]):
            centroids_broadcast = sc.broadcast(new_centroids)
        else:
            break

    with open(OUTPUT_PATH, "w") as f:
        for centroid in new_centroids:
            f.write(str(centroid) + "\n")

    execution_time = time.time() - start_time
    print("\nexecution time:", execution_time, "s")
    print("objective function value:", get_objective_function_value(new_centroids))
    print("average time per iteration:", execution_time/n, "s")
    print("n_iter:", n)
